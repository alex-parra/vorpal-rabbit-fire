var vorpal = require('vorpal')(),
  duckCount = 0,
  wabbitCount = 0;

vorpal.delimiter('rabbitFire$').show();

// duck
vorpal.command('duck', `Outputs "rabbit"`).action(function(args, callback) {
  this.log('Wabbit');
  callback();
});

vorpal.command('duck season', `Outputs "rabbit season"`).action(function(args, callback) {
  duckCount++;
  this.log('Wabbit season');
  callback();
});

// wabbit
vorpal.command('wabbit', `Outputs "duck"`).action(function(args, callback) {
  this.log('Duck');
  callback();
});

vorpal.command('wabbit season', `Outputs "duck season"`).action(function(args, callback) {
  // no cheating
  if (duckCount < 2) {
    duckCount = 0;
    this.log(`You're despicable`);
    callback();
  } else if (wabbitCount === 0) {
    wabbitCount++;
    this.log(`Duck season`);
    callback();
  } else { // doh!
    this.log(`I say it's duck season. And I say fire!`);
    vorpal.ui.cancel();
  }
});
